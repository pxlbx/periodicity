extends Control

class_name ScalableUIElement

signal scaling_changed

var current_scaling = 1
var base_props_tree = {}

func scale(scaling_factor: float):
	_scale_ui_tree(self, base_props_tree, scaling_factor)
	emit_signal('scaling_changed', scaling_factor)
	current_scaling = scaling_factor
	
func rebuild_scaling_tree():
	base_props_tree = _generate_base_props_tree()

func _ready():
	base_props_tree = _generate_base_props_tree()
	
func _generate_base_props_tree(ui_element = null):
	if ui_element == null:
		ui_element = self
	
	var props_tree = {}
	var element_class = ui_element.get_class()
	
	if element_class == 'Polygon2D':
		props_tree['position'] = ui_element.get('position')
		props_tree['scale'] = ui_element.get('scale')
		props_tree.children = {}
		return props_tree
	
	# generic Control properties
	props_tree['size'] = ui_element.get('rect_size')
	props_tree['position'] = ui_element.get('rect_position')	
	
	# specific properties
	if ui_element.get_parent() and ui_element.get_parent().get_class() == 'GridContainer':
		props_tree['min_size'] = ui_element.get('rect_min_size')
	
	if element_class == 'Panel':
		var panel_styles = ui_element.get('custom_styles/panel')
		if (panel_styles and panel_styles.get_class() == 'StyleBoxFlat'):
			props_tree['border_width_left'] = panel_styles.get('border_width_left')
			props_tree['border_width_right'] = panel_styles.get('border_width_right')
			props_tree['border_width_top'] = panel_styles.get('border_width_top')
			props_tree['border_width_bottom'] = panel_styles.get('border_width_bottom')
			props_tree['shadow_size'] = panel_styles.get('shadow_size')
			
			props_tree['corner_radius_top_left'] = panel_styles.get('corner_radius_top_left')
			props_tree['corner_radius_top_right'] = panel_styles.get('corner_radius_top_right')
			props_tree['corner_radius_bottom_left'] = panel_styles.get('corner_radius_bottom_left')
			props_tree['corner_radius_bottom_right'] = panel_styles.get('corner_radius_bottom_right')
			
	elif element_class == 'Label':
		var custom_font = ui_element.get('custom_fonts/font')
		props_tree['font_size'] = custom_font.get('size')
		props_tree['extra_spacing_bottom'] = custom_font.get('extra_spacing_bottom')
		
	elif element_class == 'GridContainer':
		if ui_element.get('custom_constants/vseparation'):
			props_tree['vseparation'] = ui_element.get('custom_constants/vseparation')
		if ui_element.get('custom_constants/hseparation'):
			props_tree['hseparation'] = ui_element.get('custom_constants/hseparation')
			
	elif element_class == 'Button':
		props_tree.font_size = ui_element.get('custom_fonts/font').get('size')
		if ui_element.get('custom_styles/hover'):
			props_tree['hover_shadow_size'] = (ui_element.get('custom_styles/hover').get('shadow_size'))
		if ui_element.get('custom_styles/normal'):
			props_tree['normal_shadow_size'] = (ui_element.get('custom_styles/normal').get('shadow_size'))
			
	# record the descendants base props recursively
	props_tree.children = {}
	for child in ui_element.get_children():
		if child.get_class() != 'Tween':
			props_tree.children[child.get_name()] = _generate_base_props_tree(child)
			
	return props_tree
	
func _scale_ui_tree(root_element, base_props, scaling_factor):
	var element_class = root_element.get_class()
	
	if element_class == 'Polygon2D':
		root_element.set('position', base_props['position'] * scaling_factor)
		root_element.set('scale', base_props['scale'] * scaling_factor)
		return
	
	root_element.set('rect_size', base_props['size'] * scaling_factor)
	root_element.set('rect_position', base_props['position'] * scaling_factor)
	
	if base_props.has('min_size'):
		root_element.set('rect_min_size', base_props['min_size'] * scaling_factor)
	
	if element_class == 'Panel':
		var panel_styles = root_element.get('custom_styles/panel')
		if (panel_styles and panel_styles.get_class() == 'StyleBoxFlat'):
			panel_styles.set('border_width_left', 
				ceil(base_props['border_width_left'] * scaling_factor))
			panel_styles.set('border_width_right', 
				ceil(base_props['border_width_right'] * scaling_factor))
			panel_styles.set('border_width_bottom', 
				ceil(base_props['border_width_bottom'] * scaling_factor))
			panel_styles.set('border_width_top', 
				ceil(base_props['border_width_top'] * scaling_factor))
			panel_styles.set('shadow_size', 
				ceil(base_props['shadow_size'] * scaling_factor))
				
			panel_styles.set('corner_radius_top_left', 
				ceil(base_props['corner_radius_top_left'] * scaling_factor))
			panel_styles.set('corner_radius_top_right', 
				ceil(base_props['corner_radius_top_right'] * scaling_factor))
			panel_styles.set('corner_radius_bottom_left', 
				ceil(base_props['corner_radius_bottom_left'] * scaling_factor))
			panel_styles.set('corner_radius_bottom_right', 
				ceil(base_props['corner_radius_bottom_right'] * scaling_factor))
		
	elif element_class == 'Label':
		var custom_font = root_element.get('custom_fonts/font')
		if base_props['extra_spacing_bottom'] != 0:
			custom_font.set('size', ceil(base_props['font_size'] * scaling_factor))
			custom_font.set('extra_spacing_bottom', ceil(base_props['extra_spacing_bottom'] * scaling_factor))
		else:
			custom_font.set('size', base_props['font_size'] * scaling_factor)
		
		
		
	elif element_class == 'GridContainer':
		if root_element.get('custom_constants/vseparation'):
			root_element.set('custom_constants/vseparation', ceil(base_props['vseparation'] * scaling_factor))
		if root_element.get('custom_constants/hseparation'):
			root_element.set('custom_constants/hseparation', ceil(base_props['hseparation'] * scaling_factor))
			
	elif element_class == 'Button':
		root_element.get('custom_fonts/font').set(
			'size', 
			base_props['font_size'] * scaling_factor
		)
		if root_element.get('custom_styles/hover'):
			root_element.get('custom_styles/hover').set(
				'shadow_size', 
				base_props['hover_shadow_size'] * scaling_factor
			)
		if root_element.get('custom_styles/normal'):
			root_element.get('custom_styles/normal').set(
				'shadow_size', 
				base_props['normal_shadow_size'] * scaling_factor
			)
	
	for child in root_element.get_children():
		if base_props.children.has(child.get_name()):
			_scale_ui_tree(child, base_props.children[child.get_name()], scaling_factor)
