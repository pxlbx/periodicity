extends ScalableUIElement

class_name Widget

# -------------------- QUICK ACCESS --------------------------- #
onready var WidgetModulator = get_node('VFX_widget_modulation')

var hub_connections_active = false

# ----------------- PUBLIC FUNCTIONS -------------------------- #

func establish_hub_connections():
	hub_connections_active = true
	
func destroy_hub_connections():
	hub_connections_active = false

func focus():
	_modulate(Color(1.0, 1.0, 1.0, 1.0))
	
func blur():
	_modulate(Color(1.0, 1.0, 1.0, 0.1))

func _modulate(modulation: Color):
	WidgetModulator.interpolate_property(
		self, 'modulate',
		self.get_modulate(), 
		modulation,
		0.1,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	WidgetModulator.start()
