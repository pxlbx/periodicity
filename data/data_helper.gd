extends Node

const ELEMENTS_DATA = preload('res://data/elements_data.gd').ELEMENTS_DATA

const ELEMENT_CLASSES_STYLES = {
	'alkali_metals': { 'background_color': Color('7C4141'), 'text_color': Color(1, 1, 1) },
	'alkaline_earth_metals': { 'background_color': Color('96524A'), 'text_color': Color(1, 1, 1) },
	'actinides': { 'background_color': Color('9C933A'), 'text_color': Color(1, 1, 1) },
	'lanthanides': { 'background_color': Color('9E7129'), 'text_color': Color(1, 1, 1) },
	'transition_metals': { 'background_color': Color('4E6C37'), 'text_color': Color(1, 1, 1) },
	'post_transition_metals': { 'background_color': Color('486A5D'), 'text_color': Color(1, 1, 1) },
	'metalloids': { 'background_color': Color('4d6264'), 'text_color': Color(1, 1, 1) },
	'other_non_metals': { 'background_color': Color('2C4349'), 'text_color': Color(1, 1, 1) },
	'noble_gases': { 'background_color': Color('5E3C58'), 'text_color': Color(1, 1, 1) },
	'unclassified': { 'background_color': Color(0.75, 0.75, 0.75), 'text_color': Color(0.2, 0.2, 0.2) }
}

const SUBLEVEL_BLOCK_STYLES = {
	's': { 'background_color': Color('904141'), 'text_color': Color(1, 1, 1) },
	'p': { 'background_color': Color('4E6C37'), 'text_color': Color(1, 1, 1) },
	'd': { 'background_color': Color('2C4349'), 'text_color': Color(1, 1, 1) },
	'f': { 'background_color': Color('5e3c58'), 'text_color': Color(1, 1, 1) },
}

const ELEMENT_CLASS_MEMBERS = {
	'alkali_metals': [3, 11, 19, 37, 55, 87],
	'alkaline_earth_metals': [4, 12, 20, 38, 56, 88],
	'actinides': [89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103],
	'lanthanides': [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71],
	'transition_metals': [21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
		39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
		72, 73, 74, 75, 76, 77, 78, 79, 80,
		104, 105, 106, 107, 108, 109, 110, 111, 112],
	'post_transition_metals': [13, 31, 49, 50, 81, 82, 83, 84],
	'metalloids': [5, 14, 32, 33, 51, 52],
	'other_non_metals': [1, 6, 7, 8, 9, 15, 16, 17, 34, 35, 53, 85],
	'noble_gases': [2, 10, 18, 36, 54, 86],
	'unclassified': [113, 114, 115, 116, 117, 118]
}

const GROUP_MEMBERS = {
	1: [1, 3, 11, 19, 37, 55, 87],
	2: [4, 12, 20, 38, 56, 88],
	3: [21, 39, 57, 89],
	4: [22, 40, 72, 104],
	5: [23, 41, 73, 105],
	6: [24, 42, 74, 106],
	7: [25, 43, 75, 107],
	8: [26, 44, 76, 108],
	9: [27, 45, 77, 109],
	10: [28, 46, 78,  110],
	11: [29, 47, 79, 111],
	12: [30, 48, 80, 112],
	13: [5, 13, 31, 49, 81, 113],
	14: [6, 14, 32, 50, 82, 114],
	15: [7, 15, 33, 51, 83, 115],
	16: [8, 16, 34, 52, 84, 116],
	17: [9, 17, 35, 53, 85, 117],
	18: [2, 10, 18, 36, 54, 86, 118]
}

const PERIOD_MEMBERS = {
	1: [1, 2],
	2: [3, 4, 5, 6, 7, 8, 9, 10],
	3: [11, 12, 13, 14, 15, 16, 17, 18],
	4: [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
		32, 33, 34, 35, 36],
	5: [37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
		50, 51, 52, 53, 54],
	6: [55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
		68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
		81, 82, 83, 84, 85, 86],
	7: [87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
		100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
		111, 112, 113, 114, 115, 116, 117, 118]
}

const SPDF_BLOCK_MEMBERS = {
	's': [1, 2, 3, 4, 11, 12, 19, 20, 37, 38, 55, 56, 87, 88],
	'p': [5, 6, 7, 8, 9, 10, 
		13, 14, 15, 16, 17, 18,
		31, 32, 33, 34, 35, 36, 
		49, 50, 51, 52, 53, 54,
		81, 82, 83, 84, 85, 86,
		113, 114, 115, 116, 117, 118],
	'd': [21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
		39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
		72, 73, 74, 75, 76, 77, 78, 79, 80,
		104, 105, 106, 107, 108, 109, 110, 111, 112],
	'f': [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
		89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103]
}

const ELEVEL_AFBAU_SEQUENCE = [1, 2, 2, 3, 3, 4, 3, 4, 5, 4, 5, 6, 4, 5, 6, 7, 5, 6, 7, 6, 7, 7]

func get_element_class(atomic_number: int):
	for classif in ELEMENT_CLASS_MEMBERS.keys():
		if ELEMENT_CLASS_MEMBERS[classif].has(atomic_number):
			return classif
	return null
	
func get_element_class_members():
	return ELEMENT_CLASS_MEMBERS
	
func get_element_class_style(classification: String):
	return ELEMENT_CLASSES_STYLES[classification]

func get_element_data(atomic_number: int):
	if atomic_number >= 1 and atomic_number <= ELEMENTS_DATA.size():
		return ELEMENTS_DATA[atomic_number - 1]
	else:
		return null
	
func get_elements_by_period(period_number: int):
	if period_number >= 1 and period_number <= 7:
		return PERIOD_MEMBERS[period_number]
	else:
		return []
		
func get_elements_by_group(group_number: int):
	if group_number >= 1 and group_number <= 18:
		return GROUP_MEMBERS[group_number]
	else:
		return []
		
func get_elements_by_class(classification: String):
	return ELEMENT_CLASS_MEMBERS[classification]
	
func get_class_display_name(classification: String):
	if classification == 'other_non_metals':
		return 'Other Non-Metals'
	elif classification == 'post_transition_metals':
		return 'Post-transition Metals'
	else:
		return classification.capitalize()
		
func get_period_of_element(atomic_number: int):
	if atomic_number >= 1 and atomic_number <= ELEMENTS_DATA.size():
		for period_number in PERIOD_MEMBERS.keys():
			if PERIOD_MEMBERS[period_number].has(atomic_number):
				return period_number

func get_group_of_element(atomic_number: int):
	if atomic_number >= 1 and atomic_number <= ELEMENTS_DATA.size():
		for group_number in GROUP_MEMBERS.keys():
			if GROUP_MEMBERS[group_number].has(atomic_number):
				return group_number

func get_spdf_block_members():
	return SPDF_BLOCK_MEMBERS
	
func get_elements_by_spdf_block(block):
	return SPDF_BLOCK_MEMBERS[block]
	
func get_spdf_block_of_element(atomic_number):
	var b = null
	for block in SPDF_BLOCK_MEMBERS.keys():
		if SPDF_BLOCK_MEMBERS[block].has(atomic_number):
			b = block
			break
	return b

func get_spdf_block_style(block: String):
	return SUBLEVEL_BLOCK_STYLES[block]

func compute_electrons_per_energy_level(electron_config: Array):
	var electrons_per_energy_level = [0, 0, 0, 0, 0, 0, 0]
	var i = 0
	for sublevel in electron_config:
		electrons_per_energy_level[ELEVEL_AFBAU_SEQUENCE[i] - 1] += sublevel
		i += 1
	return electrons_per_energy_level
