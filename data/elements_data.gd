const ELEMENTS_DATA: Array = [
	{ 
		'atomic_number': 1,
		'symbol': 'H',
		'name': 'Hydrogen',
		'atomic_mass': 1.008,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [1]
	},
	{ 
		'atomic_number': 2,
		'symbol': 'He',
		'name': 'Helium',
		'atomic_mass': 4.003,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{
		'atomic_number': 3,
		'symbol': 'Li',
		'name': 'Lithium',
		'atomic_mass': 2.001,
		'electron_config': [2, 1]
	},
	{
		'atomic_number': 4,
		'symbol': 'Be',
		'name': 'Beryllium',
		'atomic_mass': 9.012,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2, 2]
	},
	{ 
		'atomic_number': 5,
		'symbol': 'B',
		'name': 'Boron',
		'atomic_mass': 2.001,
		'electron_config': [2, 2, 1]
	},
	{ 
		'atomic_number': 6,
		'symbol': 'C',
		'name': 'Carbon',
		'atomic_mass': 12.01,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2, 2, 2]
	},
	{ 
		'atomic_number': 7,
		'symbol': 'N',
		'name': 'Nitrogen',
		'atomic_mass': 2.001,
		'electron_config': [2, 2, 3]
	},
	{ 
		'atomic_number': 8,
		'symbol': 'O',
		'name': 'Oxygen',
		'atomic_mass': 2.001,
		'electron_config': [2, 2, 4]
	},
	{ 
		'atomic_number': 9,
		'symbol': 'F',
		'name': 'Fluorine',
		'atomic_mass': 18.998,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2, 2, 5]
	},
	{ 
		'atomic_number': 10,
		'symbol': 'Ne',
		'name': 'Neon',
		'atomic_mass': 20.18,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2, 2, 6]
	},
	{ 
		'atomic_number': 11,
		'symbol': 'Na',
		'name': 'Sodium',
		'atomic_mass': 22.99,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2, 2, 6, 1]
	},
	{ 
		'atomic_number': 12,
		'symbol': 'Mg',
		'name': 'Magnesium',
		'atomic_mass': 2.001,
		'electron_config': [2, 2, 6, 2]
	},
	{ 
		'atomic_number': 13,
		'symbol': 'Al',
		'name': 'Aluminum',
		'atomic_mass': 26.98,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 14,
		'symbol': 'Si',
		'name': 'Silicon',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 15,
		'symbol': 'P',
		'name': 'Phosphorus',
		'atomic_mass': 30.974,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 16,
		'symbol': 'S',
		'name': 'Sulfur',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 17,
		'symbol': 'Cl',
		'name': 'Chlorine',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 18,
		'symbol': 'Ar',
		'name': 'Argon',
		'atomic_mass': 39.948,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 19,
		'symbol': 'K',
		'name': 'Potassium',
		'atomic_mass': 39.098,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 20,
		'symbol': 'Ca',
		'name': 'Calcium',
		'atomic_mass': 40.078,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 21,
		'symbol': 'Sc',
		'name': 'Scandium',
		'atomic_mass': 44.956,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 22,
		'symbol': 'Ti',
		'name': 'Titanium',
		'atomic_mass': 47.867,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 23,
		'symbol': 'V',
		'name': 'Vanadium',
		'atomic_mass': 50.9415,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 24,
		'symbol': 'Cr',
		'name': 'Chromium',
		'atomic_mass': 51.996,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2, 2, 6, 2, 6, 1, 5]
	},
	{ 
		'atomic_number': 25,
		'symbol': 'Mn',
		'name': 'Manganese',
		'atomic_mass': 54.938,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 26,
		'symbol': 'Fe',
		'name': 'Iron',
		'atomic_mass': 55.845,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 27,
		'symbol': 'Co',
		'name': 'Cobalt',
		'atomic_mass': 58.933,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 28,
		'symbol': 'Ni',
		'name': 'Nickel',
		'atomic_mass': 58.693,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 29,
		'symbol': 'Cu',
		'name': 'Copper',
		'atomic_mass': 63.546,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 30,
		'symbol': 'Zn',
		'name': 'Zinc',
		'atomic_mass': 65.38,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 31,
		'symbol': 'Ga',
		'name': 'Gallium',
		'atomic_mass': 69.723,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 32,
		'symbol': 'Ge',
		'name': 'Germanium',
		'atomic_mass': 72.63,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 33,
		'symbol': 'As',
		'name': 'Arsenic',
		'atomic_mass': 74.92,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 34,
		'symbol': 'Se',
		'name': 'Selenium',
		'atomic_mass': 78.972,
		'atomic_weight_source': 'isotopic abundance',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 35,
		'symbol': 'Br',
		'name': 'Bromine',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 36,
		'symbol': 'Kr',
		'name': 'Krypton',
		'atomic_mass': 83.798,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 37,
		'symbol': 'Rb',
		'name': 'Rubidium',
		'atomic_mass': 85.468,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 38,
		'symbol': 'Sr',
		'name': 'Strontium',
		'atomic_mass': 87.62,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 39,
		'symbol': 'Y',
		'name': 'Yttrium',
		'atomic_mass': 88.906,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 40,
		'symbol': 'Zr',
		'name': 'Zirconium',
		'atomic_mass': 91.224,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 41,
		'symbol': 'Nb',
		'name': 'Niobium',
		'atomic_mass': 92.906,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 42,
		'symbol': 'Mo',
		'name': 'Molybdenum',
		'atomic_mass': 95.95,
		'atomic_weight_source': 'isotopic abundance',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 43,
		'symbol': 'Tc',
		'name': 'Technetium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 44,
		'symbol': 'Ru',
		'name': 'Ruthenium',
		'atomic_mass': 101.07,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 45,
		'symbol': 'Rh',
		'name': 'Rhodium',
		'atomic_mass': 102.91,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 46,
		'symbol': 'Pd',
		'name': 'Palladium',
		'atomic_mass': 106.42,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 47,
		'symbol': 'Ag',
		'name': 'Silver',
		'atomic_mass': 107.868,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 48,
		'symbol': 'Cd',
		'name': 'Cadmium',
		'atomic_mass': 112.414,
		'atomic_weight_source': 'isotopic abundances',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 49,
		'symbol': 'In',
		'name': 'Indium',
		'atomic_mass': 114.818,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 50,
		'symbol': 'Sn',
		'name': 'Tin',
		'atomic_mass': 118.71,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 51,
		'symbol': 'Sb',
		'name': 'Antimony',
		'atomic_mass': 121.760,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 52,
		'symbol': 'Te',
		'name': 'Tellurium',
		'atomic_mass': 127.60,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 53,
		'symbol': 'I',
		'name': 'Iodine',
		'atomic_mass': 126.904,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 54,
		'symbol': 'Xe',
		'name': 'Xenon',
		'atomic_mass': 131.29,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 55,
		'symbol': 'Cs',
		'name': 'Cesium',
		'atomic_mass': 132.905,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 56,
		'symbol': 'Ba',
		'name': 'Barium',
		'atomic_mass': 137.33,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 57,
		'symbol': 'La',
		'name': 'Lanthanum',
		'atomic_mass': 138.905,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 58,
		'symbol': 'Ce',
		'name': 'Cerium',
		'atomic_mass': 140.116,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 59,
		'symbol': 'Pr',
		'name': 'Praseodymium',
		'atomic_mass': 140.908,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 60,
		'symbol': 'Nd',
		'name': 'Neodymium',
		'atomic_mass': 144.242,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 61,
		'symbol': 'Pm',
		'name': 'Promethium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 62,
		'symbol': 'Sm',
		'name': 'Samarium',
		'atomic_mass': 150.36,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 63,
		'symbol': 'Eu',
		'name': 'Europium',
		'atomic_mass': 151.964,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 64,
		'symbol': 'Gd',
		'name': 'Gadolinium',
		'atomic_mass': 157.25,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 65,
		'symbol': 'Tb',
		'name': 'Terbium',
		'atomic_mass': 158.925,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 66,
		'symbol': 'Dy',
		'name': 'Dysprosium',
		'atomic_mass': 162.500,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 67,
		'symbol': 'Ho',
		'name': 'Holmium',
		'atomic_mass': 164.930,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 68,
		'symbol': 'Er',
		'name': 'Erbium',
		'atomic_mass': 167.259,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 69,
		'symbol': 'Tm',
		'name': 'Thulium',
		'atomic_mass': 168.934,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 70,
		'symbol': 'Yb',
		'name': 'Ytterbium',
		'atomic_mass': 173.05,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 71,
		'symbol': 'Lu',
		'name': 'Lutetium',
		'atomic_mass': 174.967,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 72,
		'symbol': 'Hf',
		'name': 'Hafnium',
		'atomic_mass': 178.49,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 73,
		'symbol': 'Ta',
		'name': 'Tantalum',
		'atomic_mass': 180.948,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 74,
		'symbol': 'W',
		'name': 'Tungsten',
		'atomic_mass': 183.84,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 75,
		'symbol': 'Re',
		'name': 'Rhenium',
		'atomic_mass': 186.207,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 76,
		'symbol': 'Os',
		'name': 'Osmium',
		'atomic_mass': 190.23,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 77,
		'symbol': 'Ir',
		'name': 'Iridium',
		'atomic_mass': 192.217,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 78,
		'symbol': 'Pt',
		'name': 'Platinum',
		'atomic_mass': 195.08,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 79,
		'symbol': 'Au',
		'name': 'Gold',
		'atomic_mass': 196.966,
		'atomic_weight_source': '2012 Atomic Mass Evaluation',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 80,
		'symbol': 'Hg',
		'name': 'Mercury',
		'atomic_mass': 200.592,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 81,
		'symbol': 'Tl',
		'name': 'Thallium',
		'atomic_mass': 204.4,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 82,
		'symbol': 'Pb',
		'name': 'Lead',
		'atomic_mass': 207.2,
		'atomic_weight_source': 'Standard atomic weights 2013 table1',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 83,
		'symbol': 'Bi',
		'name': 'Bismuth',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 84,
		'symbol': 'Po',
		'name': 'Polonium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 85,
		'symbol': 'At',
		'name': 'Astatine',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 86,
		'symbol': 'Rn',
		'name': 'Radon',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 87,
		'symbol': 'Fr',
		'name': 'Francium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 88,
		'symbol': 'Ra',
		'name': 'Radium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 89,
		'symbol': 'Ac',
		'name': 'Actinium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 90,
		'symbol': 'Th',
		'name': 'Thorium',
		'atomic_mass': 232.038,
		'atomic_weight_source': 'isotopic abundance',
		'electron_config': [2]
	},
	{ 
		'atomic_number': 91,
		'symbol': 'Pa',
		'name': 'Protactinium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 92,
		'symbol': 'U',
		'name': 'Uranium',
		'atomic_mass': 2.001,
		'electron_config': [2, 2, 6, 2, 6, 2, 10, 6, 2, 10, 6, 2, 14, 10, 6, 2, 3, 1]
	},
	{
		'atomic_number': 93,
		'symbol': 'Np',
		'name': 'Neptunium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 94,
		'symbol': 'Pu',
		'name': 'Plutonium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 95,
		'symbol': 'Am',
		'name': 'Americium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 96,
		'symbol': 'Cm',
		'name': 'Curium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 97,
		'symbol': 'Bk',
		'name': 'Berkelium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 98,
		'symbol': 'Cf',
		'name': 'Californium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 99,
		'symbol': 'Es',
		'name': 'Einsteinium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 100,
		'symbol': 'Fm',
		'name': 'Fermium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 101,
		'symbol': 'Md',
		'name': 'Mendelevium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 102,
		'symbol': 'No',
		'name': 'Nobelium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 103,
		'symbol': 'Lr',
		'name': 'Lawrencium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 104,
		'symbol': 'Rf',
		'name': 'Rutherfordium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 105,
		'symbol': 'Db',
		'name': 'Dubnium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 106,
		'symbol': 'Sg',
		'name': 'Seaborgium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 107,
		'symbol': 'Bh',
		'name': 'Bohrium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 108,
		'symbol': 'Hs',
		'name': 'Hassium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 109,
		'symbol': 'Mt',
		'name': 'Meitnerium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 110,
		'symbol': 'Ds',
		'name': 'Darmstadium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 111,
		'symbol': 'Rg',
		'name': 'Roentgenium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 112,
		'symbol': 'Cn',
		'name': 'Copernicium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 113,
		'symbol': 'Nh',
		'name': 'Nihonium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 114,
		'symbol': 'Fl',
		'name': 'Flerovium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 115,
		'symbol': 'Mc',
		'name': 'Moscovium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 116,
		'symbol': 'Lv',
		'name': 'Livermorium',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 117,
		'symbol': 'Ts',
		'name': 'Tennessine',
		'atomic_mass': 2.001,
		'electron_config': [2]
	},
	{ 
		'atomic_number': 118,
		'symbol': 'Og',
		'name': 'Oganesson',
		'atomic_mass': 2.001,
		'electron_config': [2]
	}
]

