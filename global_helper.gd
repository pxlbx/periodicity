extends Node

enum MAGNIFIER_ALIGNMENT {
	CENTER, TOP, RIGHT, BOTTOM, LEFT
}
	
func access_widget(widget_id: String):
	for widget in get_tree().get_nodes_in_group('widgets'):
		if widget.WIDGET_ID == widget_id:
			return widget
			
func magnify_ui_element(
		target_node: Control, 
		magnifier_node: Control,
		target_pos_offset: Vector2,
		magnifier_alignment,
		magnifier_alignment_offset: Vector2,
		vfx_magnification: Tween
	):
	var target_pos = target_node.get('rect_position') + target_pos_offset
	var target_size = target_node.get('rect_size')
	var magnifier_size = magnifier_node.get('rect_size')
	var size_diff = magnifier_size - target_size
	var initial_pos = target_pos + ((target_size - magnifier_size * 0.5) / 2)
	var final_pos = Vector2(0, 0)
	
	if magnifier_alignment == MAGNIFIER_ALIGNMENT.CENTER:
		final_pos = target_pos - size_diff / 2
	elif magnifier_alignment == MAGNIFIER_ALIGNMENT.TOP:
		final_pos = Vector2(target_pos.x - (size_diff.x / 2), target_pos.y + size_diff.y)
	elif magnifier_alignment == MAGNIFIER_ALIGNMENT.RIGHT:
		final_pos = Vector2(target_pos.x - size_diff.x, target_pos.y - (size_diff.y / 2))
	elif magnifier_alignment == MAGNIFIER_ALIGNMENT.BOTTOM:
		final_pos = Vector2(target_pos.x - (size_diff.x / 2), target_pos.y - size_diff.y)
	elif magnifier_alignment == MAGNIFIER_ALIGNMENT.LEFT:
		final_pos = Vector2(target_pos.x, target_pos.y - (size_diff.y / 2))
		
	final_pos += magnifier_alignment_offset

	vfx_magnification.interpolate_property(
		magnifier_node, 
		'rect_scale', 
		Vector2(0.5, 0.5), 
		Vector2(1, 1), 
		0.1, 
		Tween.TRANS_LINEAR, 
		Tween.EASE_IN_OUT
	)
	
	vfx_magnification.interpolate_property(
		magnifier_node, 
		'rect_position', 
		initial_pos,
		final_pos,
		0.1,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	
	vfx_magnification.start()

func coord_within_box(coord: Vector2, box: Control):
	var startCoord = box.get('rect_position')
	var endCoord = startCoord + box.get('rect_size')
	if (coord.x >= startCoord.x and 
		coord.x <= endCoord.x  and 
		coord.y >= startCoord.y and 
		coord.y <= endCoord.y):
		return true
	else:
		return false
