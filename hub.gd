extends Node

signal scaling_changed
signal facet_changed
signal ptable_element_hovered
signal request_ptable_highlights
signal remove_ptable_highlights

const SCALING_FACTORS = [
	1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5
]
const BASE_VPORT_SIZE = Vector2(1280, 720)

const STARTING_FACET = 2
const STARTING_ELEMENT = 1

var current_scaling = 1
var current_vport_size = BASE_VPORT_SIZE

var current_facet = STARTING_FACET
var current_element = STARTING_ELEMENT

func on_element_hover(atomic_number: int):
	current_element = atomic_number
	emit_signal('ptable_element_hovered', atomic_number)
	
func on_facet_change(facet: int):
	if facet != current_facet:
		current_facet = facet
		emit_signal('facet_changed', facet)

func on_vport_resize(new_vport_size: Vector2):
	current_vport_size = new_vport_size
	var scaling_vector = new_vport_size / BASE_VPORT_SIZE
	var scaling_factor = min(scaling_vector.x, scaling_vector.y)

	# scaling below the base viewport size is not supported
	if scaling_factor < 1:
		return

	# find the nearest supported scaling factor below the actual
	var i = 0
	while i < SCALING_FACTORS.size():
		if SCALING_FACTORS[i] <= scaling_factor and SCALING_FACTORS[i + 1] > scaling_factor:
			break
		i += 1

	if SCALING_FACTORS[i] != current_scaling:
		current_scaling = SCALING_FACTORS[i]
		emit_signal('scaling_changed', current_scaling)
	
func highlight_elements_by_scheme(hlight_scheme, details):
	emit_signal('request_ptable_highlights', hlight_scheme, details)
	
func remove_ptable_highlights():
	emit_signal('remove_ptable_highlights')
