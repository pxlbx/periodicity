extends ScalableUIElement

# ----------------------- SIGNALS ----------------------------- #
signal opened
signal closed

# -------------------- CONSTS & ENUMS ------------------------- #
const TAB_NAMES = ['facet', 'settings', 'about']
const DROP_SHADOW_SIZE = 3
enum STATE { OPEN, CLOSE }

# ---------------------- RESOURCES ---------------------------- #
onready var TabStyleActive = preload('tab_styles/tab_active.tres')
onready var TabStyleInactive = preload('tab_styles/tab_inactive.tres')

# ---------------------- VFX TWEENS --------------------------- #
onready var VFX_SlideAnimation = get_node('VFX_open_close')

# --------------------- QUICK ACCESS -------------------------- #
onready var FacetTabContent = get_node('facet')

# ----------------- STATE VARIABLES --------------------------- #
var current_state = STATE.CLOSE
var tabs = []

# ------------- NATIVE GODOT ENGINE FUNCTIONS ----------------- #

func _ready():
	self.connect('scaling_changed', self, '_on_scaling_done')
	
	for tab in get_node('tabs').get_children():
		tabs.append(tab)
		tab.connect('pressed', self, '_on_tab_click', [tab])
	
	FacetTabContent.connect('facet_selected', self, '_on_facet_select')
	Hub.connect('scaling_changed', self, '_on_scaling_change')
	
	close()

# ----------------- PUBLIC FUNCTIONS -------------------------- #

func open():
	current_state = STATE.OPEN
	_show_tab_shadows()
	_animate_open_close(STATE.OPEN)
	_enable_mouse_events()
	emit_signal('opened')

func close():
	current_state = STATE.CLOSE
	_hide_tab_shadows()
	_animate_open_close(STATE.CLOSE)
	_disable_mouse_events()
	for tab in tabs:
		_gray_out_tab(tab)
	emit_signal('closed')

# ------------------- EVENT HANDLERS -------------------------- #

func _on_scaling_change(scaling_factor: float):
	scale(scaling_factor)
	set('rect_size', get('rect_size') * Vector2(1, 0) + Vector2(0, Hub.current_vport_size.y))

func _on_scaling_done(scaling_factor):
	if current_state == STATE.OPEN:
		_show_from_view()
	else:
		_hide_from_view()
		_hide_tab_shadows()
		
	self.set_anchor(MARGIN_BOTTOM, 1)
	self.get('custom_styles/panel').set(
		'shadow_size', 
		DROP_SHADOW_SIZE * scaling_factor
	)
	
func _on_tab_click(clicked_tab):
	if current_state == STATE.CLOSE:
		open()
	
	for tab in tabs:
		if tab == clicked_tab:
			_white_out_tab(tab)
		else:
			_gray_out_tab(tab)
	
	_show_tab_content(clicked_tab.get_name())

func _on_facet_select(facet_index):
	Hub.on_facet_change(facet_index)

# -------------- PRIVATE HELPER FUNCTIONS --------------------- #

func _hide_from_view():
	self.set('rect_position', Vector2(-self.get('rect_size').x, 0))
	
func _show_from_view():
	self.set('rect_position', Vector2(0, 0))

func _hide_tab_shadows():
	_set_tabs_shadow_size(0)
	
func _show_tab_shadows():
	_set_tabs_shadow_size(DROP_SHADOW_SIZE * current_scaling)

func _set_tabs_shadow_size(size: int):
	TabStyleActive.set('shadow_size', size)
	TabStyleInactive.set('shadow_size', size)

func _animate_open_close(state):
	var initial = null
	var final = null
	
	if state  == STATE.OPEN:
		initial = Vector2(-self.get('rect_size').x, 0)
		final = Vector2(0, 0)
	else:
		initial = Vector2(0, 0)
		final = Vector2(-self.get('rect_size').x, 0)
	
	VFX_SlideAnimation.interpolate_property(
		self, 'rect_position',
		initial, final,
		0.2,
		Tween.TRANS_CIRC,
		Tween.EASE_IN_OUT
	)
	VFX_SlideAnimation.start()

func _gray_out_tab(tab):
	tab.set('custom_styles/pressed', TabStyleInactive)
	tab.set('custom_styles/hover', TabStyleInactive)
	tab.set('custom_styles/normal', TabStyleInactive)

func _white_out_tab(tab):
	tab.set('custom_styles/pressed', TabStyleActive)
	tab.set('custom_styles/hover', TabStyleActive)
	tab.set('custom_styles/normal', TabStyleActive)

func _show_tab_content(tab_name):
	for t_name in TAB_NAMES:
		if t_name == tab_name:
			self.get_node(t_name).visible = true
		else:
			self.get_node(t_name).visible = false

func _enable_mouse_events():
	for t_name in TAB_NAMES:
		self.get_node(t_name).set_process_input(true)

func _disable_mouse_events():
	for t_name in TAB_NAMES:
		self.get_node(t_name).set_process_input(false)
