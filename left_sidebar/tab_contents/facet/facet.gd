extends Panel

signal facet_selected

onready var OptionActiveStyle = preload('resources/option_active.tres')
onready var OptionInactiveStyle = preload('resources/option_inactive.tres')

var option_panels = []

func _ready():
	self.get_parent().connect('scaling_changed', self, '_scale_option_borders')
	for child in self.get_children():
		if child.get_name().begins_with('option'):
			option_panels.append(child)
	
func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_LEFT:
			_on_left_click(event.position)
		
func _on_left_click(mouse_coord: Vector2):
	var selected_opt = null
	for opt in option_panels:
		if GlobalHelper.coord_within_box(mouse_coord, opt):
			emit_signal(
				'facet_selected', 
				int(opt.get_name().replace('option', ''))
			)
			_highlight_option(opt)
			selected_opt = opt
			break
	if selected_opt != null:
		for opt in option_panels:
			if opt != selected_opt:
				_unhighlight_option(opt)

func _highlight_option(option: Panel):
	option.set('custom_styles/panel', OptionActiveStyle)
	
func _unhighlight_option(option: Panel):
	option.set('custom_styles/panel', OptionInactiveStyle)

func _scale_option_borders(scaling_factor: float):
	OptionActiveStyle.set('border_width_top', 3 * scaling_factor)
	OptionActiveStyle.set('border_width_right', 3 * scaling_factor)
	OptionActiveStyle.set('border_width_bottom', 3 * scaling_factor)
	OptionActiveStyle.set('border_width_left', 3 * scaling_factor) 
	OptionActiveStyle.set('shadow_size', 4 * scaling_factor)
	
	OptionInactiveStyle.set('border_width_top', 3 * scaling_factor)
	OptionInactiveStyle.set('border_width_right', 3 * scaling_factor)
	OptionInactiveStyle.set('border_width_bottom', 3 * scaling_factor)
	OptionInactiveStyle.set('border_width_left', 3 * scaling_factor) 
