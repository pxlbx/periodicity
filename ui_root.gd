extends Control

# -------------------- QUICK ACCESS --------------------------- #
onready var LeftSidebar = get_node('left_sidebar')

# ----------------- STATE VARIABLES --------------------------- #
var left_sidebar_hover = false

# ------------- NATIVE GODOT ENGINE FUNCTIONS ----------------- #

func _ready():
	set_process_input(true)
	get_viewport().connect('size_changed', self, '_on_viewport_resize')

	LeftSidebar.connect('mouse_entered', self, '_on_left_sidebar_hover')
	LeftSidebar.connect('mouse_exited', self, '_on_left_sidebar_leave')

# ----------------- PUBLIC FUNCTIONS -------------------------- #

# ----------------- EVENT HANDLERS ------------------------- #

func _input(event):
	if (event is InputEventMouseButton 
		and event.pressed and event.button_index == BUTTON_LEFT):
			_on_left_click(event.position)

func _on_viewport_resize():
	Hub.on_vport_resize(get_viewport().size)

func _on_left_click(_mouse_coord):
	if !left_sidebar_hover and LeftSidebar.current_state == LeftSidebar.STATE.OPEN:
		LeftSidebar.close()

func _on_left_sidebar_hover():
	left_sidebar_hover = true

func _on_left_sidebar_leave():
	left_sidebar_hover = false

# ------------ PRIVATE HELPER FUNCTIONS -------------------- #
