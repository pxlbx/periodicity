extends Widget

const WIDGET_ID = 'AFBAU_PRINCIPLE'

var glow_orbs = []

func _ready():
	for child in get_children():
		if child.get_name().begins_with('glow'):
			glow_orbs.append(child)
	
func establish_hub_connections():
	.establish_hub_connections()
	display_electron_config(DataHelper.get_element_data(Hub.current_element).electron_config)
	Hub.connect('ptable_element_hovered', self, '_on_element_change')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_on_element_change')
	
func _on_element_change(atomic_number: int):
	display_electron_config(DataHelper.get_element_data(atomic_number).electron_config)

func display_electron_config(electron_config):
	var i = 0
	for sublvl in electron_config:
		if sublvl > 0:
			glow_orbs[i].set_visible(true)
		else:
			glow_orbs[i].set_visible(false)
		i += 1
	
	while i <= 18:
		glow_orbs[i].set_visible(false)
		i += 1
