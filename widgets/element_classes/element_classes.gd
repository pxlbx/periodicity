extends Widget

const WIDGET_ID = 'ELEMENT_CLASSES'

# -------------------- QUICK ACCESS --------------------------- #
onready var class_bars = [
	get_node('alkali_metals_bar'),
	get_node('alkaline_earth_metals_bar'),
	get_node('transition_metals_bar'),
	get_node('post_transition_metals_bar'),
	get_node('metalloids_bar'),
	get_node('other_non_metals_bar'),
	get_node('noble_gases_bar'),
	get_node('lanthanides_bar'),
	get_node('actinides_bar')
]
onready var LabelVertical = get_node('class_magnifier/class_name_vertical')
onready var LabelHorizontal = get_node('class_magnifier/class_name_horizontal')
onready var UnclassifiedOverlay = get_node('unclassified_overlay')

# ----------------- MAGNIFIER NODES --------------------------- #
onready var ClassMagnifier = get_node('class_magnifier')
onready var PtableWidget = GlobalHelper.access_widget('PTABLE')

# ---------------------- TWEENS ------------------------------- #
onready var ClassMagnifierTween = get_node('class_magnifier/animator')

# ----------------- STATE VARIABLES --------------------------- #
var currently_magnified_class = null
var class_magnifier_orientation = 'vertical'
var adjust_class_magnifier = false

# ------------- NATIVE GODOT ENGINE FUNCTIONS ----------------- #

func _ready():
	self.connect('scaling_changed', self, '_on_scaling_done')

	ClassMagnifier.connect('mouse_exited', self, '_on_class_magnifier_leave')
	ClassMagnifier.connect('mouse_entered', self, '_on_class_magnifier_hover')

	for class_bar in class_bars:
		class_bar.connect(
			'mouse_entered',
			self, 
			'_on_class_bar_hover', [class_bar.name.replace('_bar', '')]
		)

func _process(_delta):
	if adjust_class_magnifier == true:
		if currently_magnified_class != null:
			magnify_class(currently_magnified_class, true)
		adjust_class_magnifier = false
		
	if currently_magnified_class == null:
		magnify_class(
			DataHelper.get_element_class(
				Hub.current_element
			)
		)

# ----------------- PUBLIC FUNCTIONS -------------------------- #

func establish_hub_connections():
	.establish_hub_connections()
	Hub.connect('ptable_element_hovered', self, '_on_element_magnification')
	PtableWidget.connect('highlights_on', self, '_on_ptable_highlights_on')
	PtableWidget.connect('highlights_off', self, '_on_ptable_highlights_off')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_on_element_magnification')
	PtableWidget.disconnect('highlights_on', self, '_on_ptable_highlights_on')
	PtableWidget.disconnect('highlights_off', self, '_on_ptable_highlights_off')

func magnify_class(element_class: String, repeat_magnification = false):
	if  (element_class == null or 
		(currently_magnified_class == element_class and
		repeat_magnification == false)):
		return
	else:
		currently_magnified_class = element_class
	
	if element_class == 'unclassified':
		_show_unclassified_overlay()
		_hide_class_magnifier()
		return
	else:
		_show_class_magnifier()
		_hide_unclassified_overlay()
	
	_set_magnifier_styles(DataHelper.get_element_class_style(element_class))
	
	var class_bar = get_node(element_class + '_bar')
	var class_bar_size = class_bar.get('rect_size')
	var class_bar_pos = class_bar.get('rect_position')
	
	if element_class == 'lanthanides' or element_class == 'actinides':
		_switch_to_horizontal_magnifier(
			class_bar_size + Vector2(22, 18) * current_scaling,
			DataHelper.get_class_display_name(element_class).to_upper()
		)
	else:
		_switch_to_vertical_magnifier(
			class_bar_size + Vector2(18, 28) * current_scaling,
			DataHelper.get_class_display_name(element_class).to_upper()
		)
		
	ClassMagnifier.set('rect_position', class_bar_pos)
	
	GlobalHelper.magnify_ui_element(
		class_bar,
		ClassMagnifier,
		Vector2(0, 0),
		GlobalHelper.MAGNIFIER_ALIGNMENT.CENTER,
		Vector2(0, 0),
		ClassMagnifierTween
	)

# ----------------- EVENT HANDLERS ------------------------- #
		
func _on_scaling_done(_scaling_factor: float):
	adjust_class_magnifier = true
		
func _on_element_magnification(atomic_number):
	magnify_class(DataHelper.get_element_class(atomic_number))
	
func _on_ptable_highlights_on(highlight_type):
	if highlight_type != 'by_class':
		ClassMagnifier.visible = false

func _on_ptable_highlights_off():
	magnify_class(currently_magnified_class, true)
	
func _on_class_bar_hover(classification):
	magnify_class(classification)
	PtableWidget.highlight_elements_by_class(classification)
	
func _on_class_magnifier_leave():
	PtableWidget.cancel_highlights()
	magnify_class(
		DataHelper.get_element_class(
			Hub.current_element
		)
	)

func _on_class_magnifier_hover():
	if currently_magnified_class != null:
		PtableWidget.highlight_elements_by_class(currently_magnified_class)
	
# ------------ PRIVATE HELPER FUNCTIONS -------------------- #
	
func _set_magnifier_styles(styles):
	ClassMagnifier.get('custom_styles/panel').set_bg_color(styles.background_color)
	LabelVertical.set('custom_colors/font_color', styles.text_color)
	LabelHorizontal.set('custom_colors/font_color', styles.text_color)

func _show_class_magnifier():
	ClassMagnifier.visible = true
	
func _hide_class_magnifier():
	ClassMagnifier.visible = false
	
func _switch_to_horizontal_magnifier(magnifier_size, class_display_text):
	class_magnifier_orientation = 'horizontal'
	ClassMagnifier.set('rect_size', magnifier_size)
	LabelVertical.visible = false
	LabelHorizontal.visible = true
	LabelHorizontal.set_text(class_display_text)
	
func _switch_to_vertical_magnifier(magnifier_size, class_display_text):
	class_magnifier_orientation = 'vertical'
	ClassMagnifier.set('rect_size', magnifier_size)
	LabelVertical.visible = true
	LabelHorizontal.visible = false
	LabelVertical.set_text(class_display_text)

func _show_unclassified_overlay():
	UnclassifiedOverlay.visible = true

func _hide_unclassified_overlay():
	UnclassifiedOverlay.visible = false
