extends Widget

const WIDGET_ID = 'FULL_ELECTRON_CONFIG'

var sublevels = []
var sublevel_active_font_color = Color('2c2c2c')
var sublevel_inactive_font_color = Color('a8a8a8')

func _ready():
	for child in get_node('grid').get_children():
		sublevels.append(child)
	
func establish_hub_connections():
	.establish_hub_connections()
	set_electron_config(DataHelper.get_element_data(Hub.current_element).electron_config)
	Hub.connect('ptable_element_hovered', self, '_on_element_change')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_on_element_change')

func _on_element_change(atomic_number: int):
	set_electron_config(DataHelper.get_element_data(atomic_number).electron_config)

func set_electron_config(electron_config):
	var count = electron_config.size()
	var i = 0
	for sublevel in sublevels:
		if i < count:
			sublevel.get_node('label').set('custom_colors/font_color', sublevel_active_font_color)
			sublevel.get_node('electron_count').set_visible(true)
			sublevel.get_node('electron_count').set_text(String(electron_config[i]))
		else:
			sublevel.get_node('label').set('custom_colors/font_color', sublevel_inactive_font_color)
			sublevel.get_node('electron_count').set_visible(false)
		i += 1
