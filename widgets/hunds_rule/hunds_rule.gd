extends Widget

const WIDGET_ID = 'HUNDS_RULE'

const ORBITAL_SEQUENCE = [
	['s1'],
	['s2'],
	['p1', 'p2', 'p3'],
	['s3'],
	['p4', 'p5', 'p6'],
	['s4'],
	['d1', 'd2', 'd3', 'd4', 'd5'],
	['p7', 'p8', 'p9'],
	['s5'],
	['d6', 'd7', 'd8', 'd9', 'd10'],
	['p10', 'p11', 'p12'],
	['s6'],
	['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7'],
	['d11', 'd12', 'd13', 'd14', 'd15'],
	['p13', 'p14', 'p15'],
	['s7'],
	['f8', 'f9', 'f10', 'f11', 'f12', 'f13', 'f14'],
	['d16', 'd17', 'd18', 'd19', 'd20'],
	['p16', 'p17', 'p18']
]

onready var s_stylebox = StyleBoxFlat.new()
onready var p_stylebox = StyleBoxFlat.new()
onready var d_stylebox = StyleBoxFlat.new()
onready var f_stylebox = StyleBoxFlat.new()
onready var u_stylebox = StyleBoxFlat.new()

var orbital_sequence_nodes = []

func _ready():
	s_stylebox.set_bg_color(DataHelper.get_spdf_block_style('s').background_color)
	p_stylebox.set_bg_color(DataHelper.get_spdf_block_style('p').background_color)
	d_stylebox.set_bg_color(DataHelper.get_spdf_block_style('d').background_color)
	f_stylebox.set_bg_color(DataHelper.get_spdf_block_style('f').background_color)
	u_stylebox.set_bg_color(Color('b6b6b6'))
	
	var orbital_nodes = []
	var sub = ''
	var index = ''
	for sublevel in ORBITAL_SEQUENCE:
		for orbital_name in sublevel:
			sub = orbital_name.substr(0, 1)
			index = orbital_name.substr(1)
			orbital_nodes.append(get_node(sub).get_node(index))
		orbital_sequence_nodes.append(orbital_nodes.duplicate(true))
		orbital_nodes.clear()
	
func establish_hub_connections():
	.establish_hub_connections()
	display_electron_dist(
		DataHelper.get_element_data(Hub.current_element).electron_config
	)
	Hub.connect('ptable_element_hovered', self, '_on_element_change')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_on_element_change')
	
func _on_element_change(atomic_number: int):
	display_electron_dist(
		DataHelper.get_element_data(atomic_number).electron_config
	)
	
func display_electron_dist(electron_config):
	var i = 0
	var orbital_dist = []
	for orb_seq in orbital_sequence_nodes:
		var stylebox = u_stylebox
		var electron_count_on_sublevel = 0
		
		if i < electron_config.size() && electron_config[i] > 0:
			if ORBITAL_SEQUENCE[i][0].substr(0, 1) == 's':
				stylebox = s_stylebox
			elif ORBITAL_SEQUENCE[i][0].substr(0, 1) == 'p':
				stylebox = p_stylebox
			elif ORBITAL_SEQUENCE[i][0].substr(0, 1) == 'd':
				stylebox = d_stylebox
			elif ORBITAL_SEQUENCE[i][0].substr(0, 1) == 'f':
				stylebox = f_stylebox
			electron_count_on_sublevel = electron_config[i]
		else:
			electron_count_on_sublevel = 0
		
		var ups = min(electron_count_on_sublevel, orb_seq.size())
		var downs = electron_count_on_sublevel - ups
		
		for orb in orb_seq:
			orb.get_node('electrons/up').set_visible(false)
			orb.get_node('electrons/down').set_visible(false)
			orb.set('custom_styles/panel', u_stylebox)
			if ups > 0:
				orb.get_node('electrons/up').set_visible(true)
				orb.set('custom_styles/panel', stylebox)
				ups -= 1
			if downs > 0:
				orb.get_node('electrons/down').set_visible(true)
				downs -= 1
			
			
		orbital_dist.clear()
		i += 1
