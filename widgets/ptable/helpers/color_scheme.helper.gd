var VFXTween: Tween = null
var EcellsColors = []

func _init(vfx_tween):
	self.VFXTween = vfx_tween
	
func get_ecell_color_scheme(ecell_index):
	if EcellsColors.empty():
		return {
			'bg_color': Color(0.75, 0.75, 0.75),
			'fg_color': Color(0.2, 0.2, 0.2)
		}
	return EcellsColors[ecell_index]

func apply_ptable_color_scheme(color_scheme_index: int, ecells: Array):
	if VFXTween == null:
		return
		
	EcellsColors.clear()
	
	if color_scheme_index == 0:
		var delay = 0
		var i = 0
		for ecell in ecells:
			EcellsColors.append({
				'bg_color': Color(0.75, 0.75, 0.75),
				'fg_color': Color(0.2, 0.2, 0.2)
			})
			skin_element_cell_for_vfx(
				ecell,
				Color(0.75, 0.75, 0.75),
				Color(0.2, 0.2, 0.2),
				delay
			)
			if i % 2 == 0:
				delay += 0.007
			i += 1
			
		VFXTween.start()
		
	elif color_scheme_index == 1:
		# apply color scheme by class
		var class_members = DataHelper.get_element_class_members()
		
		for classif in class_members.keys():
			var classif_style = DataHelper.get_element_class_style(classif)
			var delay = 0
			for member in class_members[classif]:
				EcellsColors.append({
					'index': member - 1,
					'bg_color': classif_style.background_color,
					'fg_color': classif_style.text_color
				})
				skin_element_cell_for_vfx(
					ecells[member - 1],
					classif_style.background_color,
					classif_style.text_color,
					delay
				)
				delay += 0.01

		VFXTween.start()
		EcellsColors.sort_custom(self, '_compare_by_index')
		
	elif color_scheme_index == 2:
		# apply color scheme by sublevel blocks
		var sublvl_block_members = DataHelper.get_spdf_block_members()
		
		for block in sublvl_block_members.keys():
			var block_style = DataHelper.get_spdf_block_style(block)
			var delay = 0
			for member in sublvl_block_members[block]:
				EcellsColors.append({
					'index': member - 1,
					'bg_color': block_style.background_color,
					'fg_color': block_style.text_color
				})
				skin_element_cell_for_vfx(
					ecells[member - 1],
					block_style.background_color,
					block_style.text_color,
					delay
				)
				delay += 0.01
		
		VFXTween.start()
		EcellsColors.sort_custom(self, '_compare_by_index')
		
func skin_element_cell_for_vfx(ecell, bg_color, fg_color, delay = 0):
		var ecell_cstyle = ecell.get('custom_styles/panel')
		var ecell_prop_sets = ecell.get_children()
		
		# queue the background color change
		VFXTween.interpolate_property(
			ecell_cstyle, 'bg_color',
			ecell_cstyle.get('bg_color'), 
			bg_color,
			0.2,
			Tween.TRANS_LINEAR,
			Tween.EASE_OUT,
			delay
		)
		
		# apply the foreground color
		for prop_set in ecell_prop_sets:
			if prop_set.visible and prop_set.get_name().begins_with('prop_set'):
				for prop in prop_set.get_children():
					if prop.get_class() == 'Label':
						prop.set(
							'custom_colors/font_color',
							fg_color
						)

func _compare_by_index(style_a, style_b):
	if style_a.index < style_b.index:
		return true
	else:
		return false
