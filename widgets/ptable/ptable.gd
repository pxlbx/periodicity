extends Widget

const WIDGET_ID = 'PTABLE'
enum COLOR_SCHEMES {
	NEUTRAL = 0,
	ELEMENT_CLASSES = 1,
	SPDF_BLOCKS = 2
}
enum PROP_SETS {
	STANDARD = 0,
	ELECTRON_CONFIG = 1
}

# ----------------------- SIGNALS ----------------------------- #
signal highlights_on
signal highlights_off

# ------------------- EXTERNAL SCRIPTS ------------------------ #
var ColorSchemeHelper = preload('helpers/color_scheme.helper.gd')

# -------------------- QUICK ACCESS --------------------------- #
onready var ElementCells = []
onready var GroupTabs = []
onready var PeriodTabsLeft = []
onready var PeriodTabsRight = []
onready var GroupDescriptions = []
onready var ClassDescriptions = []
onready var PeriodTab1Right = get_node('ptable_grid/period_right_1')

# ----------------- MAGNIFIER NODES --------------------------- #
onready var ElementMagnifier = get_node('element_magnifier')
onready var PeriodMagnifierLeft = get_node('period_tab_magnifier_left')
onready var PeriodMagnifierRight = get_node('period_tab_magnifier_right')
onready var GroupMagnifier = get_node('group_tab_magnifier')

# ---------------------- VFX TWEENS --------------------------- #
onready var VFX_ColorScheme = get_node('VFX_color_scheme')
onready var VFX_Highlighting = get_node('VFX_highlighting')
onready var VFX_ElementMagnif = get_node('VFX_element_magnif')
onready var VFX_PeriodTabMagnif = get_node('VFX_period_tab_magnif')
onready var VFX_GroupTabMagnif = get_node('VFX_group_tab_magnif')
onready var VFX_ElementMagnifMod = get_node('VFX_element_magnif_mod')

# ----------------- STATE VARIABLES --------------------------- #
var in_highlights_mode = false
var adjust_element_magnifier = false
var currently_highlighted_elements = []
var prop_sets = []
var emagnifier_prop_sets = []

var current_prop_set = null
var current_element = 1

# ------------- NATIVE GODOT ENGINE FUNCTIONS ----------------- #

func _ready():
	# instantiate and initialize external scripts
	ColorSchemeHelper = ColorSchemeHelper.new(VFX_ColorScheme)
	
	# initialize element cells with their data
	ElementCells = get_tree().get_nodes_in_group('element_cells')
	ElementCells.sort_custom(self, '_compare_by_name')
	
	var atomic_number = 1
	var ecell_prop_sets = []
	for ecell in ElementCells:
		ecell.connect('mouse_entered', self, '_on_element_cell_hover', [atomic_number])		
		
		for child in ecell.get_children():
			if child.get_name().begins_with('prop_set'):
				child.set_visible(true)
				ecell.remove_child(child)
				ecell_prop_sets.append(child)

		prop_sets.append(ecell_prop_sets.duplicate(true))
		ecell_prop_sets.clear()
		atomic_number += 1
		
	for child in ElementMagnifier.get_children():
		if child.get_name().begins_with('prop_set'):
			emagnifier_prop_sets.append(child)
	
	PeriodTabsLeft = get_tree().get_nodes_in_group('period_tabs_left')
	var period_number = 1
	for period_tab in PeriodTabsLeft:
		period_tab.connect('mouse_entered', self, '_on_period_tab_hover', [period_number])
		period_number += 1
	
	PeriodTabsRight = get_tree().get_nodes_in_group('period_tabs_right')
	period_number = 1
	for period_tab in PeriodTabsRight:
		period_tab.connect('mouse_entered', self, '_on_period_tab_hover', [period_number])
		period_number += 1
	
	GroupTabs = get_tree().get_nodes_in_group('group_tabs')
	GroupTabs.sort_custom(self, '_compare_by_name')
	var group_number = 1
	for group_tab in GroupTabs:
		group_tab.connect('mouse_entered', self, '_on_group_tab_hover', [group_number])
		group_number += 1
		
	for child in get_node('group_descriptions').get_children():
		GroupDescriptions.append(child)
		
	for child in get_node('class_descriptions').get_children():
		ClassDescriptions.append(child)
		
	PeriodMagnifierLeft.connect('mouse_exited', self, '_on_period_magnifier_leave')
	PeriodMagnifierRight.connect('mouse_exited', self, '_on_period_magnifier_leave')
	GroupMagnifier.connect('mouse_exited', self, '_on_group_magnifier_leave')
	
func _process(_delta):
	if adjust_element_magnifier:
		magnify_element(Hub.current_element)
		adjust_element_magnifier = false
		
func _exit_tree():
	for ecell_prop_sets in prop_sets:
		for prop_set in ecell_prop_sets:
			prop_set.free()
	
	for emg_prop_set in  emagnifier_prop_sets:
		emg_prop_set.free()
	
# ----------------- PUBLIC FUNCTIONS -------------------------- #

# here, all connections to signals from the Hub are created
# this function is called externally
func establish_hub_connections():
	.establish_hub_connections()
	Hub.connect('scaling_changed', self, '_on_scaling_change')
	Hub.connect('request_ptable_highlights', self, '_on_highlight_request')
	Hub.connect('remove_ptable_highlights', self, '_on_remove_highlights_request')

# here, all connections to signals from the Hub are destroyed
# this function is called externally
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.connect('scaling_changed', self, '_on_scaling_change')
	Hub.disconnect('request_ptable_highlights', self, '_on_highlight_request')
	Hub.connect('remove_ptable_highlights', self, '_on_remove_highlights_request')

# magnifies an element specified by atomic number
# cancels any active highlights on the periodic table
func magnify_element(atomic_number: int):
	current_element = atomic_number
	Hub.on_element_hover(atomic_number)
	if current_prop_set != null:
		_init_ecell_data(
			current_prop_set, 
			emagnifier_prop_sets[current_prop_set], 
			DataHelper.get_element_data(atomic_number)	
		)
	
	_recolor_emagnifier(
		ColorSchemeHelper.get_ecell_color_scheme(atomic_number - 1)
	)

	GlobalHelper.magnify_ui_element(
		ElementCells[atomic_number - 1],
		ElementMagnifier,
		get_node('ptable_grid').get('rect_position'),
		GlobalHelper.MAGNIFIER_ALIGNMENT.CENTER,
		Vector2(0, 0),
		VFX_ElementMagnif
	)

# highlights elements specified by an array of atomic numbers
# deactivates the current element magnification
func highlight_elements(atomic_numbers: Array, by_selection = true):
	_hide_element_magnifier()
	_focus_element_cells(atomic_numbers)

	in_highlights_mode = true
	currently_highlighted_elements = atomic_numbers
	
	if by_selection:
		emit_signal('highlights_on', 'by_selection')

# highlights all elements under a group
# accepts the IUPAC group naming of 1 - 18
func highlight_elements_by_group(group_number: int):
	if group_number >= 1 and group_number <= 18:
		highlight_elements(DataHelper.get_elements_by_group(group_number), false)
		_show_group_description(group_number)
		_show_group_magnifier()
		_hide_period_magnifiers()
		_hide_all_class_descriptions()
		_magnify_group_tab(group_number)
		emit_signal('highlights_on', 'by_group')

# highlights all elements in a period
# follows the standard period naming of 1 - 7
func highlight_elements_by_period(period_number: int):
	if period_number >= 1 and period_number <= 7:
		highlight_elements(DataHelper.get_elements_by_period(period_number), false)
		_hide_all_class_descriptions()
		_hide_all_group_descriptions()
		_hide_group_magnifier()
		_show_period_magnifiers()
		_magnify_period_tab('left', period_number)
		_magnify_period_tab('right', period_number)
		emit_signal('highlights_on', 'by_period')

# highlights all elements under a classification
# example: 'lanthanides'
func highlight_elements_by_class(classification: String):
	highlight_elements(DataHelper.get_elements_by_class(classification), false)
	_show_class_descriptions(classification)
	_hide_all_group_descriptions()
	_hide_group_magnifier()
	_hide_period_magnifiers()
	emit_signal('highlights_on', 'by_class')

func highlight_elements_by_spdf_blocks(block: String):
	highlight_elements(DataHelper.get_elements_by_spdf_block(block), false)
	_hide_all_class_descriptions()
	_hide_all_group_descriptions()
	_hide_group_magnifier()
	_hide_period_magnifiers()
	emit_signal('highlights_on', 'by_spdf_block')

# cancels any currently active highlights
# reactivates the current element magnification
func cancel_highlights():
	_focus_element_cells()
	_show_element_magnifier()
	_hide_group_magnifier()
	_hide_all_group_descriptions()
	_hide_all_class_descriptions()
	_hide_period_magnifiers()
	in_highlights_mode = false
	emit_signal('highlights_off')

# get the atomic numbers of the currently highlighted elements
func get_currently_highlighted_elements():
	return currently_highlighted_elements

# sets the facet of the ptable as dictated
# this is called externally if provided (optional)
func set_facet(facet):
	cancel_highlights()
	var active_prop_set = null
	var color_scheme = null
	
	if facet == 0:
		active_prop_set = PROP_SETS.STANDARD
		color_scheme = COLOR_SCHEMES.ELEMENT_CLASSES
		current_prop_set = PROP_SETS.STANDARD
	elif facet == 1:
		active_prop_set = PROP_SETS.ELECTRON_CONFIG
		color_scheme = COLOR_SCHEMES.SPDF_BLOCKS
		current_prop_set = PROP_SETS.ELECTRON_CONFIG
	elif facet == 2:
		active_prop_set = PROP_SETS.ELECTRON_CONFIG
		color_scheme = COLOR_SCHEMES.SPDF_BLOCKS
		current_prop_set = PROP_SETS.ELECTRON_CONFIG
	else:
		print('PTABLE: facet not supported.')
		return
	
	# 1. change prop set of each element cells
	var atomic_number = 1
	for ecell in ElementCells:
		for child in ecell.get_children():
			if child.get_name().begins_with('prop_set'):
				ecell.remove_child(child)
		
		ecell.add_child(prop_sets[atomic_number - 1][active_prop_set])
		_init_ecell_data(
			active_prop_set, 
			prop_sets[atomic_number - 1][active_prop_set],
			DataHelper.get_element_data(atomic_number)
		)
		atomic_number += 1
		
	# 2. change the color scheme of the periodic table
	ColorSchemeHelper.apply_ptable_color_scheme(color_scheme, ElementCells)
		
	# 3. change prop set of ElementMagnifier
	for child in ElementMagnifier.get_children():
		if child.get_name().begins_with('prop_set'):
			ElementMagnifier.remove_child(child)
			
	ElementMagnifier.add_child(emagnifier_prop_sets[active_prop_set])
	emagnifier_prop_sets[active_prop_set].set_visible(true)
	magnify_element(current_element)
	
# ----------------- EVENT HANDLERS ------------------------- #
		
func _on_element_cell_hover(atomic_number: int):
	if in_highlights_mode:
		cancel_highlights()
	Hub.on_element_hover(atomic_number)
	magnify_element(atomic_number)
	
func _on_scaling_change(_scaling_factor):
	adjust_element_magnifier = true

func _on_highlight_request(hlight_scheme, details):
	if hlight_scheme == 'by_spdf_blocks':
		highlight_elements_by_spdf_blocks(details)

func _on_remove_highlights_request():
	cancel_highlights()

func _on_period_tab_hover(period_number: int):
	highlight_elements_by_period(period_number)

func _on_period_magnifier_leave():
	cancel_highlights()
	
func _on_group_tab_hover(group_number: int):
	highlight_elements_by_group(group_number)
	
func _on_group_magnifier_leave():
	cancel_highlights()

# ------------ PRIVATE HELPER FUNCTIONS -------------------- #

func _compare_by_name(node_a, node_b):
	if int(node_a.get_name()) < int(node_b.get_name()):
		return true
	else:
		return false

func _focus_element_cells(selection = null):
	var atomic_number = 1
	var modulation = null
	for ecell in ElementCells:
		if selection != null:
			if selection.has(atomic_number):
				modulation = Color(1.0, 1.0, 1.0, 1.0)
			else:
				modulation = Color(1.0, 1.0, 1.0, 0.1)
		else:
			modulation = Color(1.0, 1.0, 1.0, 1.0) 
		VFX_Highlighting.interpolate_property(
			ecell, 'modulate',
			ecell.get_modulate(), 
			modulation,
			0.2,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN_OUT
		)
		atomic_number += 1
	VFX_Highlighting.start()

func _hide_element_magnifier():
	ElementMagnifier.set('mouse_filter', MOUSE_FILTER_IGNORE)
	VFX_ElementMagnifMod.interpolate_property(
		ElementMagnifier, 'modulate',
		ElementMagnifier.get_modulate(), Color(1, 1, 1, 0), 0.1,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	VFX_ElementMagnifMod.start()
	
func _show_element_magnifier():
	ElementMagnifier.set('mouse_filter', MOUSE_FILTER_STOP)
	VFX_ElementMagnifMod.interpolate_property(
		ElementMagnifier, 'modulate',
		ElementMagnifier.get_modulate(), Color(1, 1, 1, 1.0), 0.1,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	VFX_ElementMagnifMod.start()
	
func _magnify_period_tab(side: String, period_number: int):
	var tab = null
	var magnifier = null
	var magnifier_align = null
	var magnifier_align_offset = null
	
	PeriodMagnifierLeft.get_node('number').set_text(String(period_number))
	PeriodMagnifierRight.get_node('number').set_text(String(period_number))
	
	if side == 'left':
		tab = PeriodTabsLeft[period_number - 1]
		magnifier = PeriodMagnifierLeft
		magnifier_align = GlobalHelper.MAGNIFIER_ALIGNMENT.RIGHT
		magnifier_align_offset = Vector2(2 * current_scaling, 0)
	else:
		tab = PeriodTabsRight[period_number - 1]
		magnifier = PeriodMagnifierRight
		magnifier_align = GlobalHelper.MAGNIFIER_ALIGNMENT.LEFT
		magnifier_align_offset = Vector2(- 2 * current_scaling, 0)
	
	GlobalHelper.magnify_ui_element(
		tab,
		magnifier,
		get_node('ptable_grid').get('rect_position'),
		magnifier_align,
		magnifier_align_offset,
		VFX_PeriodTabMagnif
	)
	
func _magnify_group_tab(group_number: int):
	var target_tab = GroupTabs[group_number - 1]
	var target_pos_offset = (target_tab.get_parent().get('rect_position') 
		+ get_node('ptable_grid').get('rect_position'))
	
	# set the number of the GroupTabMagnifier
	GroupMagnifier.get_node('number').set_text(String(group_number))
		
	GlobalHelper.magnify_ui_element(
		target_tab,
		GroupMagnifier,
		target_pos_offset,
		GlobalHelper.MAGNIFIER_ALIGNMENT.BOTTOM,
		Vector2(0, 2 * current_scaling),
		VFX_GroupTabMagnif
	)

func _show_period_magnifiers():
	PeriodMagnifierLeft.visible = true
	PeriodMagnifierRight.visible = true
	
func _hide_period_magnifiers():
	PeriodMagnifierLeft.visible = false
	PeriodMagnifierRight.visible = false
	
func _show_group_magnifier():
	GroupMagnifier.visible = true
	
func _hide_group_magnifier():
	GroupMagnifier.visible = false

func _show_group_description(group_number: int):
	if group_number > 18:
		return
	var g = 1
	for group_desc in GroupDescriptions:
		if g == group_number:
			group_desc.visible = true
		else:
			group_desc.visible = false
		g += 1
	
func _hide_all_group_descriptions():
	for group_desc in GroupDescriptions:
		group_desc.visible = false

func _show_class_descriptions(classification: String):
	for class_desc in ClassDescriptions:
		if class_desc.name == classification:
			class_desc.visible = true
		else:
			class_desc.visible = false

func _hide_all_class_descriptions():
	for class_desc in ClassDescriptions:
		class_desc.visible = false

#func _busy_mode_on():
#	busy_mode = true
	
#func _busy_mode_off():
#	busy_mode = false

func _init_ecell_data(prop_set, prop_set_tree, element_data):
	if prop_set == PROP_SETS.STANDARD:
		prop_set_tree.get_node('atomic_number').set_text(String(element_data.atomic_number))
		prop_set_tree.get_node('symbol').set_text(element_data.symbol)
		prop_set_tree.get_node('name').set_text(element_data.name)
		prop_set_tree.get_node('atomic_weight').set_text(String(element_data.atomic_mass))
	elif prop_set == PROP_SETS.ELECTRON_CONFIG:
		prop_set_tree.get_node('atomic_number').set_text(String(element_data.atomic_number))
		prop_set_tree.get_node('symbol').set_text(element_data.symbol)
		prop_set_tree.get_node('name').set_text(element_data.name)
		var energy_levels = ''
		var electrons_per_elevel = DataHelper.compute_electrons_per_energy_level(element_data.electron_config)
		for e in electrons_per_elevel:
			if e > 0:
				energy_levels = energy_levels + String(e) + '\n'
		prop_set_tree.get_node('energy_levels').set_text(energy_levels)
	else:
		return

func _recolor_emagnifier(colors):
	ElementMagnifier.get('custom_styles/panel').set_bg_color(colors.bg_color)
	for prop_set in ElementMagnifier.get_children():
		for prop in prop_set.get_children():
			if prop.get_class() == 'Label':
				prop.set(
					'custom_colors/font_color',
					colors.fg_color
				)
