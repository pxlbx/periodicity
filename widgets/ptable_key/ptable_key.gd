extends Widget

const WIDGET_ID = 'PTABLE_KEY'

var prop_sets = []

# -------------------- QUICK ACCESS --------------------------- #
onready var BgStyles = get_node('bg').get('custom_styles/panel')

# ------------- NATIVE GODOT ENGINE FUNCTIONS ----------------- #

func _ready():	
	for child in get_children():
		if child.get_name().begins_with('prop_set'):
			child.set_visible(true)
			prop_sets.append(child)
			remove_child(child)

func _exit_tree():
	for prop_set in prop_sets:
		prop_set.free()

# ----------------- PUBLIC FUNCTIONS ------------------------- #

func establish_hub_connections():
	.establish_hub_connections()
	Hub.connect('ptable_element_hovered', self, '_show_element_info')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_show_element_info')

func _show_element_info(atomic_number: int):
	var element_data = DataHelper.get_element_data(atomic_number)
	var prop_set = null
	
	if Hub.current_facet == 0:
		var element_styles = DataHelper.get_element_class_style(
			DataHelper.get_element_class(atomic_number)
		)
		prop_set = prop_sets[0]
		prop_set.get_node('prop_atomic_number').set_text(String(element_data.atomic_number))
		prop_set.get_node('prop_symbol').set_text(element_data.symbol)
		prop_set.get_node('prop_name').set_text(element_data.name)
		prop_set.get_node('prop_atomic_weight').set_text(String(element_data.atomic_mass))
		BgStyles.set_bg_color(element_styles.background_color)
		_change_fg_color(prop_set, element_styles.text_color)
		
	elif Hub.current_facet == 1 || Hub.current_facet == 2:
		var element_styles = DataHelper.get_spdf_block_style(
			DataHelper.get_spdf_block_of_element(atomic_number)
		)
		prop_set = prop_sets[1]
		prop_set.get_node('prop_atomic_number').set_text(String(element_data.atomic_number))
		prop_set.get_node('prop_symbol').set_text(element_data.symbol)
		prop_set.get_node('prop_name').set_text(element_data.name)
		var energy_levels = ''
		var electrons_per_elevel = DataHelper.compute_electrons_per_energy_level(element_data.electron_config)
		for e in electrons_per_elevel:
			if e > 0:
				energy_levels = energy_levels + String(e) + '\n'
		prop_set.get_node('prop_energy_levels').set_text(energy_levels)
		BgStyles.set_bg_color(element_styles.background_color)
		_change_fg_color(prop_set, element_styles.text_color)

func set_facet(facet: int):
	for child in get_children():
		if child.get_name().begins_with('prop_set'):
			remove_child(child)
			
	if facet == 0:
		add_child(prop_sets[0])
	elif facet == 1:
		add_child(prop_sets[1])
	elif facet == 2:
		add_child(prop_sets[1])
	else:
		print('PTABLE KEY: facet not supported.' )
		return
	
	_show_element_info(Hub.current_element)

func _change_fg_color(prop_set, fg_color):
	for child in prop_set.get_children():
		if child.get_class() == 'Label' and child.get_name().begins_with('prop'):
			child.set('custom_colors/font_color', fg_color)
		else:
			_change_fg_color(child, fg_color)
