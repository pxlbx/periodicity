extends Widget

const WIDGET_ID = 'SUBLEVEL_BLOCKS'

onready var VFX_magnif_effect = get_node('VFX_magnif_effect')
onready var bar_magnifier = get_node('bar_magnifier')
onready var bar_magnifier_styles = bar_magnifier.get('custom_styles/panel')

var adjust_magnifier = false
var currently_magnified_bar = null

func _ready():
	for child in get_children():
		if child.get_name().ends_with('bar'):
			child.connect('mouse_entered', self, '_on_bar_hover', [child])
			
	connect('scaling_changed', self, '_on_scaling_changed')
	bar_magnifier.connect('mouse_exited', self, '_on_magnifier_leave')
	
func _process(_delta):
	if adjust_magnifier && currently_magnified_bar != null:
		_magnify_block_bar(currently_magnified_bar, true)
		adjust_magnifier = false

func establish_hub_connections():
	.establish_hub_connections()
	var sub = DataHelper.get_spdf_block_of_element(Hub.current_element)
	_magnify_block_bar(get_node(sub + '_bar'))
	Hub.connect('ptable_element_hovered', self, '_on_element_change')
	
func destroy_hub_connections():
	.destroy_hub_connections()
	Hub.disconnect('ptable_element_hovered', self, '_on_element_change')		
	
func _on_element_change(atomic_number: int):
	var sub = DataHelper.get_spdf_block_of_element(atomic_number)
	_magnify_block_bar(get_node(sub + '_bar'))

func _on_bar_hover(block_bar):
	_magnify_block_bar(block_bar)
	Hub.highlight_elements_by_scheme('by_spdf_blocks', block_bar.get_name().substr(0, 1))
	
func _on_magnifier_leave():
	Hub.remove_ptable_highlights()
	var sub = DataHelper.get_spdf_block_of_element(Hub.current_element)
	_magnify_block_bar(get_node(sub + '_bar'))
	
func _on_scaling_changed(_scaling_factor):
	adjust_magnifier = true
	
func _magnify_block_bar(block_bar, force_magnif = false):
	if block_bar != currently_magnified_bar or force_magnif == true:
		currently_magnified_bar = block_bar
	else:
		 return
		
	GlobalHelper.magnify_ui_element(
		block_bar, 
		bar_magnifier,
		Vector2(0, 0),
		GlobalHelper.MAGNIFIER_ALIGNMENT.CENTER,
		Vector2(0, 0),
		VFX_magnif_effect
	)
	bar_magnifier.get_node('label').set_text(block_bar.get_node('label').get_text())
	var bg_color = block_bar.get('custom_styles/panel').get_bg_color()
	bar_magnifier_styles.set_bg_color(bg_color)

