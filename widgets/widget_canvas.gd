extends Panel

var widgets = {}
var facet_configs = [
	# facet 0
	{
		'PTABLE': Vector2(0, 15),
		'PTABLE_KEY': Vector2(-290, -225),
		'ELEMENT_CLASSES': Vector2(-15, -240)
	},
	# facet 1
	{
		'PTABLE': Vector2(-40, 15),
		'PTABLE_KEY': Vector2(-330, -225),
		'AFBAU_PRINCIPLE': Vector2(-70, -225),
		'SUBLEVEL_BLOCKS': Vector2(-525, 260),
		'FULL_ELECTRON_CONFIG': Vector2(560, 20)
	},
	# facet 2
	{
		'PTABLE': Vector2(-40, 15),
		'PTABLE_KEY': Vector2(-330, -225),
		'HUNDS_RULE': Vector2(-68, -230),
		'SUBLEVEL_BLOCKS': Vector2(-525, 260),
		'FULL_ELECTRON_CONFIG': Vector2(560, 20)
	}
]

func _ready():
	for widget in get_tree().get_nodes_in_group('widgets'):
		widgets[widget.WIDGET_ID] = widget
	
#	_arrange_widgets(Hub.current_scaling, Hub.current_vport_size, Hub.current_facet)
	_apply_facet_config(Hub.current_facet)
	
	Hub.connect('scaling_changed', self, '_on_scaling_change')
	Hub.connect('facet_changed', self, '_on_facet_change')
	
func _exit_tree():
	for widget_key in widgets.keys():
		if !is_a_parent_of(widgets[widget_key]):
			widgets[widget_key].free()
	
func _on_facet_change(facet):
	_apply_facet_config(facet)

func _on_scaling_change(scaling_factor):
	for child in get_children():
		if child is Widget:
			
			child.scale(scaling_factor)
			_position_widget(
				child, 
				scaling_factor, 
				Hub.current_vport_size, 
				facet_configs[Hub.current_facet][child.WIDGET_ID]
			)

func _apply_facet_config(facet):
	var scaling_factor = Hub.current_scaling
	var vport_size = Hub.current_vport_size
	var fconfig = facet_configs[facet]
	
	for widget_id in widgets.keys():
		var widget = widgets[widget_id]
		if fconfig.has(widget_id):
			if !is_a_parent_of(widget):
				add_child(widget)
			if widget.has_method('set_facet'):
				widget.set_facet(facet)
			
			widget.scale(scaling_factor)
			_position_widget(widget, scaling_factor, vport_size, fconfig[widget.WIDGET_ID])
			
			if !widget.hub_connections_active:
				widget.establish_hub_connections()
		else:
			if is_a_parent_of(widget):
				remove_child(widget)
			if widget.hub_connections_active:
				widget.destroy_hub_connections()
	
func _position_widget(widget, scaling_factor, vport_size, center_offset):
	var centered_position_x = (vport_size.x - widget.get('rect_size').x) / 2
	var centered_position_y = (vport_size.y - widget.get('rect_size').y) / 2
	widget.set(
		'rect_position', 
		Vector2(centered_position_x, centered_position_y) + center_offset * scaling_factor
	)
	widget.set_visible(true)
